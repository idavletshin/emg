FROM node:12-alpine
WORKDIR /app
COPY package*.json ./
COPY .env.* ./
COPY .eslintrc.js ./
COPY .prettierrc ./
COPY .stylelintrc.json ./
COPY babel.config.js ./
COPY vue.config.js ./
RUN npm ci
COPY public public
COPY src src
RUN npm install -g @vue/cli
RUN npm run build

CMD npm run serve



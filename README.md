# emg-front

## Project setup fro development
Install all the dependencies.
```
npm install
```
copy hostname to your hosts file
```
127.0.0.1               emg.local
```

Create new .env file named .env.development.local for local development

```
touch .env.development.local

echo "NODE_ENV=development
VUE_APP_ENVIRONMENT=development
VUE_APP_API_HOST=apiHOST" >> .env.development.local
```

Run server to watch and recompile your code.
```
npm run serve
```

Run build to build for production.
```
npm run build
```

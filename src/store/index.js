import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);
import ui from './modules/ui';

const persistedStateOptions = {
    paths: ['ui'],
};

const store = new Vuex.Store({
    strict: process.env.NODE_ENV !== 'production',
    plugins: [createPersistedState(persistedStateOptions)],
    modules: {
        ui,
    },
});

if (module.hot) {
    module.hot.accept(['./modules/ui'], () => {
        const newUi = require('./modules/ui').default;

        store.hotUpdate({
            modules: {
                ui: newUi,
            },
        });
    });
}

export default store;

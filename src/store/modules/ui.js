import { SIDEBAR_TOGGLE, CHAT_TOGGLE } from '@/store/constants';

const toggle = (key) => (state, payload) => {
    state[key] = payload ? payload : !state[key]
};

const state = {
    sidebarCollapsed: false,
    chatCollapsed: false,
};
const actions = {};

const mutations = {
    [SIDEBAR_TOGGLE]: (state, payload) => {
        state.sidebarCollapsed = payload;
    },
    [CHAT_TOGGLE]: toggle('chatCollapsed')
};

const getters = {};

export default {
    state,
    actions,
    mutations,
    getters,
};

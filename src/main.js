import Vue from 'vue';
import App from './App.vue';
import DefaultLayout from '@ui/layouts/Default';
import { Btn, SvgIcon } from '@ui/atoms';
import './registerServiceWorker';
import router from './router';
import store from './store';

Vue.component('default-layout', DefaultLayout);
Vue.component('Btn', Btn);
Vue.component('SvgIcon', SvgIcon);

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');

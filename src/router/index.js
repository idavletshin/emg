import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import('@pages/Home/Home'),
    },
    {
        path: '/jackpot',
        name: 'jackpot',
        component: () => import('@pages/Jackpot/Jackpot'),
    },
    {
        path: '/double',
        name: 'double',
        component: () => import('@pages/Double/Double'),
    },
    {
        path: '/crash',
        name: 'crash',
        component: () => import('@pages/Crash/Crash'),
    },
    {
        path: '/nvuti',
        name: 'nvuti',
        component: () => import('@pages/Nvuti/Nvuti'),
    },
    {
        path: '/withdrawal',
        name: 'withdrawal',
        component: () => import('@pages/Withdrawal/Withdrawal'),
    },
    {
        path: '/replenishment',
        name: 'replenishment',
        component: () => import('@pages/Replenishment/Replenishment'),
    },
    {
        path: '/referrals',
        name: 'referrals',
        component: () => import('@pages/Referrals/Referrals'),
    },
    {
        path: '/transfer',
        name: 'transfer',
        component: () => import('@pages/Transfer/Transfer'),
    },
    {
        path: '/bonuses',
        name: 'bonuses',
        component: () => import('@pages/Bonuses/Bonuses'),
    },
    {
        path: '/exit',
        name: 'exit',
        component: () => import('@pages/Exit/Exit'),
    },
    {
        path: '/landing',
        name: 'landing',
        meta: { layout: 'div' },
        component: () => import('@pages/Landing/landing'),
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    linkActiveClass: 'nav-item-active',
    routes,
});

export default router;
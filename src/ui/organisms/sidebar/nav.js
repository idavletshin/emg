import jackpot from '@/assets/svg/jackpot.svg';
// import jackpot from '@/assets/svg/jackpot_noactive.svg';
// import double from '@/assets/svg/double.svg';
import double from '@/assets/svg/double_noactive.svg';
// import crash from '@/assets/svg/crash.svg';
import crash from '@/assets/svg/crash_noactive.svg';
// import nvuti from '@/assets/svg/nvuti.svg';
import nvuti from '@/assets/svg/nvuti_noactive.svg';

import replenishment from '@/assets/svg/replenishment.svg';
import withdrawal from '@/assets/svg/withdrawal.svg';
import referrals from '@/assets/svg/referrals.svg';
import transfer from '@/assets/svg/transfer.svg';
import bonuses from '@/assets/svg/bonuses.svg';
import exit from '@/assets/svg/exit.svg';


const navs = [
    {
        routeName: 'jackpot',
        icon: jackpot,
        title: 'Jackpot game',
        path: 'Jackpot',
        isHighlighted: true,
    },
    {
        routeName: 'double',
        icon: double,
        title: 'Double game',
        path: 'Double',
        isHighlighted: true,
    },
    {
        routeName: 'crash',
        icon: crash,
        title: 'Crash game',
        path: 'Crash',
        isHighlighted: true,
    },
    {
        routeName: 'nvuti',
        icon: nvuti,
        title: 'Nvuti game',
        path: 'Nvuti',
        isHighlighted: true,
    },
    {
        routeName: 'replenishment',
        icon: replenishment,
        title: 'Пополнение',
        path: 'Nvuti',
    },
    {
        routeName: 'withdrawal',
        icon: withdrawal,
        title: 'Вывод',
        path: 'Nvuti',
    },
    {
        routeName: 'transfer',
        icon: transfer,
        title: 'Перевод',
        path: 'Nvuti',
    },
    {
        routeName: 'referrals',
        icon: referrals,
        title: 'Рефералам',
        path: 'Nvuti',
    },
    {
        routeName: 'bonuses',
        icon: bonuses,
        title: 'Бонусы',
        path: 'Nvuti',
    },
    {
        routeName: 'exit',
        icon: exit,
        title: 'Выход',
        path: 'Nvuti',
        onClick: () => {
            console.log('exit');
        },
    },
];
export { navs };

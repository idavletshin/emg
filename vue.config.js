const TerserPlugin = require('terser-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const isProd = process.env.NODE_ENV === 'production';
const path = require('path');

module.exports = {
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "@/ui/global/variables.scss";`,
            },
        },
    },
    lintOnSave: !isProd,
    configureWebpack: {
        // plugins: [
        //     new StyleLintPlugin({
        //         files: ['src/**/*.{vue,htm,html,css,sss,less,scss,sass}'],
        //     }),
        // ],
        resolve: {
            alias: {
                '@pages': path.resolve(__dirname, 'src/pages'),
                '@components': path.resolve(__dirname, 'src/components'),
                '@ui': path.resolve(__dirname, 'src/ui'),
                '@libs': path.resolve(__dirname, 'src/libs'),
            },
        },
        performance: {
            hints: isProd ? false : 'warning',
        },
        optimization: isProd
            ? {
                  minimize: true,
                  minimizer: [
                      new TerserPlugin({
                          parallel: true,
                          terserOptions: {
                              extractComments: 'all',
                              compress: {
                                  drop_console: true,
                              },
                          },
                      }),
                  ],
              }
            : {},
    },
    devServer: {
        host: 'localhost',
        port: '8080',
    },
};
